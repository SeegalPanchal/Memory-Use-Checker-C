/*
Name: Seegal Panchal
ID:   1016249
Date: 03/05/2018

This file contains the functions that replace memory allocationg
and free calls, and analyzes them to find any memory leaks

*/

#ifndef MEMCHECK_H
#define MEMCHECK_H

#include <stdio.h>
#include <stdlib.h>

/* macros that rename wherever these calls appear
with the ones on the right (call user created functions instead)
__FILE__ = the file it was called (built in macro)
__LINE__ = the line it was called (built in macro) */
#define malloc(size)          memcheck_malloc(size,__FILE__,__LINE__)
#define free(ptr)             memcheck_free(ptr,__FILE__,__LINE__)
#define calloc(nmemb,size)    memcheck_calloc(nmemb,size,__FILE__,__LINE__)
#define realloc(ptr,size)     memcheck_realloc(ptr,size,__FILE__,__LINE__)
#define main()                memcheck_main()

/* prototypes for the functions */
void *memcheck_malloc(size_t size, char *file, int line);
void memcheck_free(void *ptr, char *file, int line);
void *memcheck_calloc(size_t nmemb, size_t size, char *file, int line);
void *memcheck_realloc(void *ptr, size_t size, char *file, int line);
int memcheck_main();

#endif
