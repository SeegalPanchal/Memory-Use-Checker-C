/*
Name: Seegal Panchal
ID:   1016249
Date: 03/05/2018

This file contains the functions that replace memory allocationg
and free calls, and analyzes them to find any memory leaks

*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

/* the head pointer that points to
the beginning of the list */
struct List *global_ptr = NULL;

/* list structure that contains
the ptr to memory being allocated,
file, where the memory allocation or free was called,
the line it was called,
and a pointer to the next struct in the linked list */
struct List {
  void *ptr;
  char *file;
  int line;
  void *next;
};

/* function prototypes */
int memcheck_main();
char *strdup2(char *str);
void print_list(struct List *linked_list);

int main() {
  void *temp_ptr;
  memcheck_main();

  temp_ptr = global_ptr;
  print_list(temp_ptr);

  return 0;
}

/* call this instead of malloc,
what you do is you malloc some memory,
of the size that the user inputted
and record the file and line of the malloc malloc
call into a linked list structure */
void *memcheck_malloc(size_t size, char *file, int line) {
  /* malloc what main in main.c was trying to malloc  */
  void *temp = malloc(size);
  /* make a list structure */
  struct List *linked_list = malloc(sizeof(struct List));
  /* point to address that was malloc'd */
  linked_list->ptr = temp;
  /* strdup2 the file */
  linked_list->file = strdup2(file);
  /* copy the line value */
  linked_list->line = line;
  /* make the next point to the global variable */
  linked_list->next = global_ptr;
  /* make the global variable point to the new list ptr */
  global_ptr = linked_list;
  /*
  printf("In malloc: File = \"%s\", Line = %d\n", file, line);
  */
  return temp;
}

/* look through the linked list and see if
the memory that is being freed has been allocated
if no memory has been allocated, print an error message
saying that you are freeing unallocated memory */
void memcheck_free(void *ptr, char *file, int line) {

  /* intially point to the first one */
  struct List *previous = NULL;
  struct List *current = NULL;

  current = global_ptr;
  previous = current;

  while (current != NULL) {
    if (current->ptr == ptr) {
      if (current == global_ptr) {
        global_ptr = global_ptr->next;
      }
      /* make the previous node point to node after the current node */
      previous->next = current->next;
      /* make the current node point to NULL */
      current->next = NULL;
      /* free the ptr */
      free(ptr);
      /* free the character pointer that you allocate with strdup 2 */
      free(current->file);
      /* free the current node */
      free(current);
      /* return from the function */
      return;
    } else {
      /* make the current node the tail */
      previous = current;
      /* check the next node now */
      current = current->next;
    }
  }

  /* the while loop ran, and we didnt return,
  which means unallocated memory is being freed */
  printf("memcheck error:  attempting to free memory address %p in file \"%s\", line %d.\n", (void *)ptr, file, line);
  return;
}

/* copy of malloc basically, instead of calling
malloc, call calloc and return a ptr to the allocated
memory, while recording where it was calling
inside a structure and add it to the linked list */
void *memcheck_calloc(size_t nmemb, size_t size, char *file, int line) {
  /* malloc what main in main.c was trying to malloc  */
  void *temp = calloc(nmemb, size);
  /* make a list structure */
  struct List *linked_list = malloc(sizeof(struct List));
  /* point to address that was malloc'd */
  linked_list->ptr = temp;
  /* strdup2 the file */
  linked_list->file = strdup2(file);
  /* copy the line value */
  linked_list->line = line;
  /* make the next point to the global variable */
  linked_list->next = global_ptr;
  /* make the global variable point to the new list ptr */
  global_ptr = linked_list;
  /*
  printf("In malloc: File = \"%s\", Line = %d\n", file, line);
  */
  return temp;
}

/* check the list if there is any allocated memory,
if there is, then edit the list, otherwise, allocate
the memory and return a pointer to the memory to the user */
void *memcheck_realloc(void *ptr, size_t size, char *file, int line) {

  /* intially point to the first element */
  struct List *current = global_ptr;
  /* look through the linked list */
  while (current != NULL) {
    if (current->ptr == ptr) {
      /* realloc like the user wanted initially */
      ptr = realloc(ptr, size);
      /* set the new pointer in the linked list */
      current->ptr = ptr;
      /* free the current file */
      free(current->file);
      /* set the new file realloc was called in the linked list */
      current->file = strdup2(file);
      /* set the line realloc was called */
      current->line = line;
      /* return the starting location of reallocd memory */
      return ptr;
    }
    /* increment through the linked list */
    else {
      /* check the next node now */
      current = current->next;
    }
  }

  /* if we haven't returned anything, it's because the memory
  hasn't been malloc'd and a person is calling realloc instead
  of mallocing first and is just calling realloc instantly

  malloc the memory (add to the linked list) */
  ptr = memcheck_malloc(size, file, line);
  /* return the pointer to the memory to the user */
  return ptr;
}

/* print the list (from header, so it will print backwards)*/
void print_list(struct List *linked_list) {
  /* while linked list is not NULL */
  while (linked_list != NULL) {
    printf("memcheck error:  memory address %p which was allocated in file \"%s\", line %d, was never freed\n", (void *)linked_list->ptr, linked_list->file, linked_list->line);
    linked_list = linked_list->next;
  }
  return;
}

/* allocate memory for string and a null terminating char */
char *strdup2(char *str) {
  char *new;
  new = malloc( strlen(str)+1 );
  if (new)
    strcpy( new, str );
  return new;
}
