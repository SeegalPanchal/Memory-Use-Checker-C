#include "memcheck.h"

int main() {
  void *ptr = malloc(1);
  free(ptr);

  return 0;
}
